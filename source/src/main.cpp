#include <stdio.h>
#include "stm32f30x.h"
#include "debug_trace.h"
#ifdef USE_DBGUART
#include "dev_uart.h"
#endif
#ifdef USE_STTERM
#include "stlinky.h"
#endif
#include "mod_led.h"
#include "timer_sched.h"

#include "digit.h"
#if defined(COMP_MODEL)
#include "model_data_compressed.h"
#else
#include "model_data_uncompressed.h"
#endif

#include "tensorflow/lite/micro/kernels/all_ops_resolver.h"
#include "tensorflow/lite/micro/micro_error_reporter.h"
#include "tensorflow/lite/micro/micro_interpreter.h"
#include "tensorflow/lite/version.h"
#include "schema_generated.h"

#include "mnist_schema_generated.h"

#define LED_TIMER_MS 500
#define LED_PORT GPIOC
#define LED_PIN GPIO_Pin_13

#define CODE_VERSION 100
#define FB_UART_BUFFER_SIZE 1
#define TENSOR_ARENA_SIZE 8200 //(262144 / 8)

volatile uint32_t glb_tmr_1ms;
volatile uint32_t glb_tmr_1sec = 0;
float glb_inference_time_ms = 0;
uint32_t trace_levels;

/* Create the list head for the timer */
static LIST_HEAD(obj_timer_list);

// Declare uart
#ifdef USE_DBGUART
DECLARE_UART_DEV(dbg_uart, USART1, 115200, 256, 10, 1);
#endif

#ifdef USE_SEMIHOSTING
extern void initialise_monitor_handles(void);
#endif

#ifdef USE_OVERCLOCKING
extern uint32_t overclock_stm32f303(void);
#endif

struct tflite_model {
    const tflite::Model* model;
    tflite::ErrorReporter* error_reporter;
    tflite::MicroInterpreter* interpreter;
    TfLiteTensor* input;
    TfLiteTensor* output;
    int inference_count;
    uint8_t tensor_arena[TENSOR_ARENA_SIZE];
};
struct tflite_model tf;

static inline void main_loop(void)
{
	/* 1 ms timer */
	if (glb_tmr_1ms) {
		glb_tmr_1ms = 0;
		mod_timer_polling(&obj_timer_list);
	}
}

void led_on(void *data)
{
	LED_PORT->ODR |= LED_PIN;
}

void led_off(void *data)
{
	LED_PORT->ODR &= ~LED_PIN;
}

void led_init(void *data)
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = LED_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(LED_PORT, &GPIO_InitStructure);

	LED_PORT->ODR |= LED_PIN;
	TRACE(("init\n"));
}

uint32_t disableInts(void)
{
    uint32_t state;

    state = __get_PRIMASK();
    __disable_irq();

    return state;
}

void restoreInts(uint32_t state)
{
   __set_PRIMASK(state);
}

void ViewModel(struct tflite_model *tf)
{
    TfLiteTensor *input = tf->interpreter->input(0);
    TfLiteTensor *output = tf->interpreter->output(0);

    TRACE(("Model input:\n"));
    TRACE(("dims->size: %d\n", input->dims->size));
    TRACE(("dims->data[0]: %d\n", input->dims->data[0]));
    TRACE(("dims->data[1]: %d\n", input->dims->data[1]));
    TRACE(("dims->data[2]: %d\n", input->dims->data[2]));
    TRACE(("dims->data[3]: %d\n", input->dims->data[3]));
    TRACE(("input->type: %d\n\n", input->type));

    TRACE(("Model output:\n"));
    TRACE(("dims->size: %d\n", output->dims->size));
    TRACE(("dims->data[0]: %d\n", output->dims->data[0]));
    TRACE(("dims->data[1]: %d\n\n", output->dims->data[1]));
}

void CmdSendStats()
{
    flatbuffers::FlatBufferBuilder fbb;

    MnistProt::StatsBuilder stats_builder(fbb);
    stats_builder.add_version(CODE_VERSION);
    stats_builder.add_freq(SystemCoreClock);
    stats_builder.add_mode(MnistProt::Mode_ACCELERATION_CMSIS_NN);
    auto stats = stats_builder.Finish();

    MnistProt::CommandsBuilder builder(fbb);
    builder.add_cmd(MnistProt::Command_CMD_GET_STATS);
    builder.add_stats(stats);
    auto resp = builder.Finish();

    fbb.Finish(resp);
    
    uint8_t *buf = fbb.GetBufferPointer();
    int buf_size = fbb.GetSize();

    for (int i=0; i<buf_size; i++) {
        TRACE(("%02X,", buf[i]));
    }
    TRACE(("\n"));
    TRACE(("Sending: %d\n", buf_size));
    // HAL_UART_Transmit(&huart7, (uint8_t *)buf, buf_size, 100);
}

__attribute__((section(".ccmram")))
void RunInference(struct tflite_model *tf, float *data, size_t data_size, uint8_t debug)
{
    // // Obtain pointers to the model's input and output tensors
    TfLiteTensor *input = tf->interpreter->input(0);
    TfLiteTensor *output = tf->interpreter->output(0);

    /* Copy data to the input buffer. So much wasted RAM! */
    for (size_t i = 0; i < data_size; i++) {
        input->data.f[i] = data[i];
    }

    if (debug) TRACE(("Running inference...\n"));

    uint32_t ints = disableInts();
    glb_inference_time_ms = 0;
    // Run the model on this input and make sure it succeeds.
    TfLiteStatus invoke_status = tf->interpreter->Invoke();
    if (invoke_status != kTfLiteOk) {
        tf->error_reporter->Report("Invoke failed\n");
    }
    restoreInts(ints);

	// flatbuffers::FlatBufferBuilder fbb;
    // auto out_vect = fbb.CreateVector((float*) output->data.f, 10);
    // auto output_f = MnistProt::CreateInferenceOutput(fbb, out_vect, 0, glb_inference_time_ms);

    // MnistProt::CommandsBuilder builder(fbb);
    // builder.add_cmd(MnistProt::Command_CMD_INFERENCE_OUTPUT);
    // builder.add_ouput(output_f);
    // auto resp = builder.Finish();
    // fbb.Finish(resp);

    // uint8_t *buf = fbb.GetBufferPointer();
    // int buf_size = fbb.GetSize();

    // HAL_UART_Transmit(&huart7, (uint8_t *)buf, buf_size, 10);

    if (debug) {
        TRACE(("Done in %f msec...\n", glb_inference_time_ms));
        for (size_t i = 0; i < 10; i++) {
            TRACE(("Out[%d]: %f\n", i, output->data.f[i]));
        }
    }
}

int main(void)
{
#ifdef USE_OVERCLOCKING
    SystemCoreClock = overclock_stm32f303();
#endif
	if (SysTick_Config(SystemCoreClock / 1000)) {
		/* Capture error */
		while (1);
	}

    trace_levels_set(
			0
			| TRACE_LEVEL_DEFAULT
			,1);

#ifdef USE_SEMIHOSTING
	initialise_monitor_handles();
#elif USE_STTERM
	stlinky_init();
#elif USE_DBGUART
	// setup uart port
	dev_uart_add(&dbg_uart);
	// set callback for uart rx
 	dbg_uart.fp_dev_uart_cb = NULL;
 	mod_timer_add(reinterpret_cast<void*>(&dbg_uart), 5, reinterpret_cast<void (*)(void *)>(&dev_uart_update), &obj_timer_list);
#endif

	// /* Declare LED module and initialize it */
	// DECLARE_MODULE_LED(led_module, 8, 250);
	// mod_led_init(&led_module);
	// mod_timer_add((void*) &led_module, led_module.tick_ms, reinterpret_cast<void (*)(void *)>(&mod_led_update), &obj_timer_list);

	// /* Declare LED */
	// DECLARE_DEV_LED(def_led, &led_module, 1, NULL, &led_init, &led_on, &led_off);
	// dev_led_add(&def_led);
	// dev_led_set_pattern(&def_led, 0b11001100);

	TRACE(("Program started\n"));

	
    // Set up logging
    tflite::MicroErrorReporter micro_error_reporter;
    tf.error_reporter = &micro_error_reporter;

    // Map the model into a usable data structure. This doesn't involve any
    // copying or parsing, it's a very lightweight operation.
    tf.model = tflite::GetModel(jupyter_notebook_mnist_tflite);
    if (tf.model->version() != TFLITE_SCHEMA_VERSION) {
    	TF_LITE_REPORT_ERROR(tf.error_reporter,
                         "Model provided is schema version %d not equal "
                         "to supported version %d.",
                         tf.model->version(), TFLITE_SCHEMA_VERSION);
    }

	// This pulls in all the operation implementations we need.
	// NOLINTNEXTLINE(runtime-global-variables)
	static tflite::ops::micro::AllOpsResolver resolver;

    // Build an interpreter to run the model with
    tf.interpreter = new tflite::MicroInterpreter(tf.model, resolver, tf.tensor_arena, TENSOR_ARENA_SIZE, tf.error_reporter);

	// Allocate memory from the tensor_arena for the model's tensors.
	TfLiteStatus allocate_status = tf.interpreter->AllocateTensors();
	if (allocate_status != kTfLiteOk) {
		TF_LITE_REPORT_ERROR(tf.error_reporter, "AllocateTensors() failed");
	}
    tf.inference_count = 0;

    RunInference(&tf, (float *)digit, 784, 1);

	TRACE(("here\n"));
	/* main loop */
	while (1) {
		main_loop();
	}
}